import os
import sys
import time
import math
import numpy as np
import qrcode
import torch
import torch.nn as nn
import torch.nn.init as init
from torch.autograd import Variable
from matplotlib import pylab
from scipy.ndimage.interpolation import rotate
from PIL import Image
import cv2
#_, term_width = os.popen('stty size', 'r').read().split()
term_width = 80

TOTAL_BAR_LENGTH = 35.
last_time = time.time()
begin_time = last_time
def progress_bar(current, total, msg=None):
    global last_time, begin_time
    if current == 0:
        begin_time = time.time()  # Reset for new bar.

    cur_len = int(TOTAL_BAR_LENGTH*current/total)
    rest_len = int(TOTAL_BAR_LENGTH - cur_len) - 1

    if msg:
        sys.stdout.write(msg+' | ')

    sys.stdout.write(' [')
    for i in range(cur_len):
        sys.stdout.write('=')
    sys.stdout.write('>')
    for i in range(rest_len):
        sys.stdout.write('.')
    sys.stdout.write(']')

    cur_time = time.time()
    step_time = cur_time - last_time
    last_time = cur_time
    tot_time = cur_time - begin_time

    L = []
    L.append(' %d/%d' % (current+1, total))
    L.append(' | Step: %s' % format_time(step_time))
    L.append(' | Tot: %s' % format_time(tot_time))

    msg = ''.join(L)
    sys.stdout.write(msg)

    if current < total-1:
        sys.stdout.write('\r')
    else:
        sys.stdout.write('\n')
    sys.stdout.flush()

def format_time(seconds):
    days = int(seconds / 3600/24)
    seconds = seconds - days*3600*24
    hours = int(seconds / 3600)
    seconds = seconds - hours*3600
    minutes = int(seconds / 60)
    seconds = seconds - minutes*60
    secondsf = int(seconds)
    seconds = seconds - secondsf
    millis = int(seconds*1000)

    f = ''
    i = 1
    if days > 0:
        f += str(days) + 'D'
        i += 1
    if hours > 0 and i <= 2:
        f += str(hours) + 'h'
        i += 1
    if minutes > 0 and i <= 2:
        f += str(minutes) + 'm'
        i += 1
    if secondsf > 0 and i <= 2:
        f += str(secondsf) + 's'
        i += 1
    if millis > 0 and i <= 2:
        f += str(millis) + 'ms'
        i += 1
    if f == '':
        f = '0ms'
    return f


def submatrix(arr):
    x, y = np.nonzero(arr)
    # Using the smallest and largest x and y indices of nonzero elements, 
    # we can find the desired rectangular bounds.  
    # And don't forget to add 1 to the top bound to avoid the fencepost problem.
    return arr[x.min():x.max()+1, y.min():y.max()+1]


class ToSpaceBGR(object):
    def __init__(self, is_bgr):
        self.is_bgr = is_bgr
    def __call__(self, tensor):
        if self.is_bgr:
            new_tensor = tensor.clone()
            new_tensor[0] = tensor[2]
            new_tensor[2] = tensor[0]
            tensor = new_tensor
        return tensor


class ToRange255(object):
    def __init__(self, is_255):
        self.is_255 = is_255
    def __call__(self, tensor):
        if self.is_255:
            tensor.mul_(255)
        return tensor


def init_patch_circle(image_size, patch_size, urlString):
    image_size = image_size**2
    noise_size = int(image_size*patch_size)
    radius = int(math.sqrt(noise_size/math.pi))
    patch = np.zeros((1, 3, radius*2, radius*2))
    qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_H,
    box_size=10,
    border=4,
    )	
    qr = qrcode.make(urlString) #URL string goes here, creates 1-bit (0,1) Image
    # Reduce QR size to 0.** of patch size to increase adversarial area
    qrSize = int(np.floor(radius*2*0.85))
    if(qrSize % 2 == 1): qrSize -= 1
    qr = qr.resize((qrSize,qrSize))
    qr.save("test_normal.png")
    indexQR = np.array(qr).astype(np.uint8) #Keep this line if removing padding
    print("QR shape before padding: ", indexQR.shape)
    # Pad the QR to be = patch size
    padSize = int(((radius*2)-qrSize)/2)
    indexQR = np.pad(indexQR, ((padSize,padSize),(padSize,padSize)), 'constant', constant_values=(1))
    print("QR shape after padding: ", indexQR.shape)

    for i in range(3):
        a = np.zeros((radius*2, radius*2))    
        cx, cy = radius, radius # The center of circle 
        y, x = np.ogrid[-radius: radius, -radius: radius]
        index = x**2 + y**2 <= radius**2
        print("Index ", i, " shape: ", index.shape)

        a[cy-radius:cy+radius, cx-radius:cx+radius][index] = np.random.rand()
        idx = np.flatnonzero((a == 0).all((1)))
        a = np.delete(a, idx, axis=0)
        patch[0][i] = np.delete(a, idx, axis=1)

        #Added: Multiply patch w/ QR to make circular patch
        patch[0][i] = patch[0][i] * indexQR

    #Apply patch to new array for saving
    img = np.zeros((radius*2, radius*2, 3))
    img[:,:,0] = patch[0][0]
    img[:,:,1] = patch[0][1]
    img[:,:,2] = patch[0][2]
    print("Final patch shape: ", patch[0][0].shape)
    print("Final image shape: ", img.shape)
    png = Image.fromarray((img * 255).astype(np.uint8))
    png.save("init_patch_circle.png")

    return patch, patch.shape


def circle_transform(patch, data_shape, patch_shape, image_size):
    # get dummy image 
    x = np.zeros(data_shape)
   
    # get shape
    m_size = patch_shape[-1]
    
    for i in range(x.shape[0]):

        # random rotation (360 degree rotation = QR not transparent)
        #print(m_size)
        """
        rot = np.random.choice(4)
        img = np.zeros((m_size, m_size, 3))
        img[:,:,0] = patch[0][0]
        img[:,:,1] = patch[0][1]
        img[:,:,2] = patch[0][2]
        rows,cols = img.shape[0], img.shape[1]
        M = cv2.getRotationMatrix2D((rows/2,cols/2),rot*90,1)
        dst = cv2.warpAffine(img,M,(rows,cols))
        patch[0][0] = dst[:,:,0]
        patch[0][1] = dst[:,:,1]
        patch[0][2] = dst[:,:,2]
        """
        """
        for j in range(patch[i].shape[0]):
            #print(i," ",j) #(0,0),(0,1),(0,2)
            patch[i][j] = rotate(patch[i][j], angle=rot, reshape=False)
        """
        
        rot = np.random.choice(4)
        for j in range(patch[i].shape[0]):
            patch[i][j] = np.rot90(patch[i][j], rot)
        
        
        # random location
        random_x = np.random.choice(image_size)
        if random_x + m_size > x.shape[-1]:
            while random_x + m_size > x.shape[-1]:
                random_x = np.random.choice(image_size)
        random_y = np.random.choice(image_size)
        if random_y + m_size > x.shape[-1]:
            while random_y + m_size > x.shape[-1]:
                random_y = np.random.choice(image_size)
       
        # apply patch to dummy image  
        x[i][0][random_x:random_x+patch_shape[-1], random_y:random_y+patch_shape[-1]] = patch[i][0]
        x[i][1][random_x:random_x+patch_shape[-1], random_y:random_y+patch_shape[-1]] = patch[i][1]
        x[i][2][random_x:random_x+patch_shape[-1], random_y:random_y+patch_shape[-1]] = patch[i][2]
    
    mask = np.copy(x)
    mask[mask != 0] = 1.0
    #print(mask.shape)
    #print(x.shape)
    #img = Image.fromarray(np.swapaxes(x[0,:,:,:], 0, 2))
    #img.show()
    return x, mask, patch.shape


def init_patch_square(image_size, patch_size, urlString):
    # get mask
    image_size = image_size**2
    noise_size = image_size*patch_size
    noise_dim = int(noise_size**(0.5))
    radius = int(math.sqrt(noise_size/math.pi))
    patch = np.zeros((1, 3, radius*2, radius*2))
    qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_H,
    box_size=10,
    border=4,
    )
    for i in range(3):
        a = np.zeros((radius*2, radius*2))    
        cx, cy = radius, radius # The center of circle 
        y, x = np.ogrid[-radius: radius, -radius: radius]
        index = x**2 + y**2 <= radius**2
        print(index.shape)
        #print(index)
        qr = qrcode.make(urlString) #URL string goes here
        qr = qr.resize((radius*2,radius*2))
        index2 = np.array(qr)
        #index3 = np.pad(index2[3:-3,3:-3], pad_width=((3, 3), (3, 3)), mode='constant', constant_values=False)
        #index2 = np.invert(index3)
        indices = index2.astype(np.uint8)  #convert to an unsigned byte
        indices*=255
        cv2.imwrite('test_normal.png', indices) # save generated qr
        #cv2.waitKey(10000)
        print(index2.shape)
        #print(index2)
        index = index2
        a[cy-radius:cy+radius, cx-radius:cx+radius][index] = np.random.rand()
        idx = np.flatnonzero((a == 0).all((1)))
        a = np.delete(a, idx, axis=0)
        patch[0][i] = np.delete(a, idx, axis=1)
    
    #Apply patch to new array for saving
    img = np.zeros((radius*2, radius*2, 3))
    img[:,:,0] = patch[0][0]
    img[:,:,1] = patch[0][1]
    img[:,:,2] = patch[0][2]
    print("Patch shape: ", patch[0][0].shape)
    print("Image shape: ", img.shape)
    png = Image.fromarray((img * 255).astype(np.uint8))
    png.save("init_patch_square.png")
    return patch, patch.shape


def square_transform(patch, data_shape, patch_shape, image_size):
    # get dummy image [3,299,299]
    x = np.zeros(data_shape)
    
    # get shape
    m_size = patch_shape[-1]
    
    for i in range(x.shape[0]):

        # random rotation
        rot = np.random.choice(4)
        for j in range(patch[i].shape[0]):
            patch[i][j] = np.rot90(patch[i][j], rot)
        
        # random location
        random_x = np.random.choice(image_size)
        if random_x + m_size > x.shape[-1]:
            while random_x + m_size > x.shape[-1]:
                random_x = np.random.choice(image_size)
        random_y = np.random.choice(image_size)
        if random_y + m_size > x.shape[-1]:
            while random_y + m_size > x.shape[-1]:
                random_y = np.random.choice(image_size)
       
        # apply patch to dummy image  
        x[i][0][random_x:random_x+patch_shape[-1], random_y:random_y+patch_shape[-1]] = patch[i][0]
        x[i][1][random_x:random_x+patch_shape[-1], random_y:random_y+patch_shape[-1]] = patch[i][1]
        x[i][2][random_x:random_x+patch_shape[-1], random_y:random_y+patch_shape[-1]] = patch[i][2]
    
    mask = np.copy(x)
    mask[mask != 0] = 1.0
    
    return x, mask

'''
if __name__ == '__main__':
    patch, patch_shape = init_patch_circle(299, 0.09, 'http://clockup.net') 
    patch, mask, patch_shape = circle_transform(patch, (1,3,299,299), patch_shape, 299)
    #patch, patch_shape = init_patch_square(299, 0.05, 'http://clockup.net') 
'''
