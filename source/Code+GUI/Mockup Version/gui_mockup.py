from subprocess import PIPE, STDOUT, Popen
from queue import Queue, Empty
import threading
import tkinter as tk
from PIL import Image, ImageTk
from tkinter import messagebox, scrolledtext
from tkinter.filedialog import askdirectory
from tkinter.ttk import *
import qrcode
import cv2
import math
import numpy as np
import time

training = False
def gocallback():
    ## Callback method for the "Go" button: Executes training code using settings from the GUI.
    global training 
    training = True
    full = "python3 -u make_patch.py " +\
        "--workers " + txtWorkers.get() + " " +\
        "--epochs " + txtEpochs.get() + " " +\
        ("--cuda " if chkCUDAState.get() else "") +\
        "--target " + (txtImgClass.get().split(':')[0] if len(txtImgClass.get().split(':')) == 2 else txtImgClass.get()) + " " +\
        "--conf_target " + txtConfTarget.get() + " " +\
        "--max_count " + txtMaxCount.get() + " " +\
        "--patch_type square " +\
        "--patch_size " + txtPatchSize.get() + " " +\
        "--train_size " + txtTrainSize.get() + " " +\
        "--test_size " + txtTestSize.get() + " " +\
        "--image_size " + txtImgSize.get() + " " +\
        "--plot_all " + ("1" if chkPlotState.get() == 1 else "0") + " " +\
        "--netClassifier " + comboClassifier.get() + " " +\
        ("--outf " + txtOutput.get() + " " if chkPlotState.get() == 1 else "") +\
        "--manualSeed " + txtSeed.get() + " "\
        "--urlString " + comboURL.get()

    # Execute the training process using a thread
    t = threading.Thread(target=execute, args=[full])
    t.start()


stop_event = threading.Event()
def execute(cmd):
    ## Execute the training process and retrieving sysout into the console
	## Clear the console before starting
    console.config(state=tk.NORMAL)
    console.tag_configure("last_insert")
    console.delete('1.0', tk.END)
    console.insert(tk.END, ("> "+cmd+'\n'))
    console.config(state=tk.DISABLED)
    ## Execute the training code as child process
    mock_print = [line.rstrip('\n') for line in open('./mock_print.txt')]
    ## Print out lines from make_patch.py while the child process is not terminated
    for line in mock_print:
        ## If the signal from "Terminate" button is received, terminate the child process
        if stop_event.is_set():
            console.config(state=tk.NORMAL)
            console.insert(tk.END, ("> Terminating process by command...\n"))
            console.see(tk.END)
            console.config(state=tk.DISABLED)
            break
        ## Print out the line into the console
        if line != '':
            console.config(state=tk.NORMAL)
            last_insert = console.tag_ranges("last_insert")
            ## If the line is a progressbar, delete and replace onto the same line
            if 'Step:' in line and last_insert:
                last_line = console.get(last_insert[0], last_insert[1])
                if 'Step:' in last_line:
                    console.delete(last_insert[0], last_insert[1])
            console.tag_remove("last_insert", "1.0", "end")
            console.insert(tk.END, ("> "+line+"\n"), "last_insert")
            console.see(tk.END)
            console.update_idletasks()
            console.config(state=tk.DISABLED)
            time.sleep(0.25)
    stop_event.clear()
    ## Send termination confirmation message to console
    console.config(state=tk.NORMAL)
    console.insert(tk.END, ("> Process "+cmd.split()[2]+" terminated\n"))
    console.see(tk.END)
    console.config(state=tk.DISABLED)

    ## Display trained QR patch
    top = tk.Toplevel()
    load = Image.open("qrpresets/"+comboURL.get().split(".")[1]+".png")
    render = ImageTk.PhotoImage(load)
    lblResult = Label(top, image = render)
    lblResult.image = render
    lblResult.grid()
    global training 
    training = False

    
def terminatecallback():
    ## Callback for "Terminate button": send Event flag to the child process to terminate it
    stop_event.set()
    console.config(state=tk.NORMAL)
    console.insert(tk.END, ("> Termination signal sent. Please wait until the process is successfully terminated...\n"))
    console.see(tk.END)
    console.config(state=tk.DISABLED)


def on_keyrelease(event):
    # Image class listbox: get all classes containing string in Entry
    # get text from entry
    value = event.widget.get()
    value = value.strip().lower()
    # get data from test_list
    if value == '':
        data = test_list
    else:
        data = []
        for item in test_list:
            if value in item.lower():
                data.append(item)
    # update data in listbox
    listbox_update(data)


def listbox_update(data):
    # Image class listbox: update listbox entries according to Entry
    # delete previous data
    listImgClass.delete(0, 'end')
    # put new data
    for item in data:
        listImgClass.insert('end', item)


def on_select(event):
    # Image class listbox: get selected class from Listbox to Entry
    # display element selected on list
    txtImgClass.delete(0, tk.END)
    selection = event.widget.curselection() # gets class idx
    if selection:
        txtImgClass.insert(0, event.widget.get(event.widget.curselection()))


def validate_int(p):
    ## Entry input validation: validate if the Entry is an integer or is empty
    if str.isdigit(p) or p == "":
        return True
    else:
        return False


def validate_float(action, text, p):
    ## Entry input validation: validate if the Entry is a float
    if action == '1':
        if text in '0123456789.' or len(text) > 1:
            try:
                float(p)
                return True
            except ValueError:
                return False
        else:
            return False
    else:
        return True


def activate_check():
    ## Checkbox callback for chkPlotState
    if chkPlotState.get() == 1:  # whenever checked
        txtOutput.config(state=tk.NORMAL)
        btnOutput.config(state=tk.NORMAL)
    elif chkPlotState.get() == 0:  # whenever unchecked
        txtOutput.config(state=tk.DISABLED)
        btnOutput.config(state=tk.DISABLED)
		

def empty_check(event):
    ## Checks if any of the input fields are unfilled in order to disable the "Go" button.
    filled = txtWorkers.get() and txtEpochs.get() and txtMaxCount.get() and txtConfTarget.get() and txtImgSize.get() and txtPatchSize.get() and txtTrainSize.get() and txtTestSize.get() and txtSeed.get()
    global training
    if chkPlotState.get() == 1:
        filled = filled and txtOutput.get()

    if filled and not training:
        btnGo.config(state=tk.NORMAL)
        btnTerminate.config(state=tk.DISABLED)
    else:
        btnGo.config(state=tk.DISABLED)
        btnTerminate.config(state=tk.NORMAL)


def get_directory():
    ## Callback for the "Browse..." button: get folder directory
    path = askdirectory(title='Select Folder')  # shows dialog box and return the path
    if path:
        txtOutput.delete(0, tk.END)
        txtOutput.insert(tk.END, path)


def comboURLCallback(event):
    ## Callback for every time the URL value changes: update the QR preview
    qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_H,
    box_size=10,
    border=4,
    )
    qr = qrcode.make(comboURL.get())
    qr = qr.resize((128, 128), Image.ANTIALIAS)
    render = ImageTk.PhotoImage(qr)
    lblImg.config(image=render)
    lblImg.image = render


## Main window
window = tk.Tk()
window.title("Adversarial QR Application")
window.bind("<Key>", empty_check)
# window.geometry('640x480')
lblMain = Label(window, text="Adversarial QR Application", font=("Arial Bold", 20))
lblMain.grid(column=0, row=0, columnspan=5, padx=10, pady=10)
valInt = window.register(validate_int)
valFloat = window.register(validate_float)

## 1. Worker threads
lblWorkers = Label(window, text="Number of worker threads:")
lblWorkers.grid(column=0, row=1, sticky="W", padx=10, pady=5)
txtWorkers = Entry(window, width=20, validate='all', validatecommand=(valInt, '%P'))
txtWorkers.insert(tk.END, '2')
txtWorkers.grid(column=1, row=1, sticky="W", padx=10, pady=5)

## 2. Enable CUDA
lblCUDA = Label(window, text="Enable CUDA?")
lblCUDA.grid(column=2, row=1, sticky="W", padx=10, pady=5)
chkCUDAState = tk.BooleanVar()
chkCUDAState.set(True)
chkCUDA = Checkbutton(window, var=chkCUDAState)
chkCUDA.grid(column=3, row=1, sticky="W", padx=85, pady=5)

## 3. Training epochs
lblEpochs = Label(window, text="Training epochs:")
lblEpochs.grid(column=0, row=2, sticky="W", padx=10, pady=5)
txtEpochs = Entry(window, width=20, validate='all', validatecommand=(valInt, '%P'))
txtEpochs.insert(tk.END, '20')
txtEpochs.grid(column=1, row=2, sticky="W", padx=10, pady=5)

## 4. Max iteration count
lblMaxCount = Label(window, text="Max iterations per epoch:")
lblMaxCount.grid(column=2, row=2, sticky="W", padx=10, pady=5)
txtMaxCount = Entry(window, width=20, validate='all', validatecommand=(valInt, '%P'))
txtMaxCount.insert(tk.END, '1000')
txtMaxCount.grid(column=3, row=2, sticky="W", padx=10, pady=5)


## 5. Image size
lblImgSize = Label(window, text="Image size (height & width):")
lblImgSize.grid(column=0, row=3, sticky="W", padx=10, pady=5)
txtImgSize = Entry(window, width=20, validate='all', validatecommand=(valInt, '%P'))
txtImgSize.insert(tk.END, '299')
txtImgSize.grid(column=1, row=3, sticky="W", padx=10, pady=5)

## 6. Patch size
lblPatchSize = Label(window, text="Patch size (% of image):")
lblPatchSize.grid(column=2, row=3, sticky="W", padx=10, pady=5)
txtPatchSize = Entry(window, width=20, validate='all', validatecommand=(valFloat, '%d', '%S', '%P'))
txtPatchSize.insert(tk.END, '0.05')
txtPatchSize.grid(column=3, row=3, sticky="W", padx=10, pady=5)

## 7. Target confidence level
lblConfTarget = Label(window, text="Target confidence level:")
lblConfTarget.grid(column=0, row=4, sticky="W", padx=10, pady=5)
txtConfTarget = Entry(window, width=20, validate='all', validatecommand=(valFloat, '%d', '%S', '%P'))
txtConfTarget.insert(tk.END, '0.9')
txtConfTarget.grid(column=1, row=4, sticky="W", padx=10, pady=5)

## 8. Target classifier network
lblClassifier = Label(window, text="Target classifier network:")
lblClassifier.grid(column=2, row=4, sticky="W", padx=10, pady=5)
comboClassifier = Combobox(window, state="readonly", width=17)
comboClassifier['values'] = ("inceptionv3", "densenet201", "resnet101")
comboClassifier.current(0)  # set the selected item
comboClassifier.grid(column=3, row=4, sticky="W", padx=10, pady=5)

## 9. Training images
lblTrainSize = Label(window, text="Number of training images:")
lblTrainSize.grid(column=0, row=5, sticky="W", padx=10, pady=5)
txtTrainSize = Entry(window, width=20, validate='all', validatecommand=(valInt, '%P'))
txtTrainSize.insert(tk.END, '2000')
txtTrainSize.grid(column=1, row=5, sticky="W", padx=10, pady=5)

## 10. Testing images
lblTestSize = Label(window, text="Number of testing images:")
lblTestSize.grid(column=2, row=5, sticky="W", padx=10, pady=5)
txtTestSize = Entry(window, width=20, validate='all', validatecommand=(valInt, '%P'))
txtTestSize.insert(tk.END, '2000')
txtTestSize.grid(column=3, row=5, sticky="W", padx=10, pady=5)

## 11. Manual seed
lblSeed = Label(window, text="Manual randomization seed:")
lblSeed.grid(column=0, row=6, sticky="W", padx=10, pady=5)
txtSeed = Entry(window, width=20, validate='all', validatecommand=(valInt, '%P'))
txtSeed.insert(tk.END, '1338')
txtSeed.grid(column=1, row=6, sticky="W", padx=10, pady=5)

## 12. Plot successful images
lblPlotImg = Label(window, text="Plot successful adversarial images?")
lblPlotImg.grid(column=0, row=7, sticky="W", padx=10, pady=5)
chkPlotState = tk.BooleanVar()
chkPlotState.set(True)
chkPlotImg = Checkbutton(window, var=chkPlotState, command=activate_check)
chkPlotImg.grid(column=1, row=7, sticky="W", padx=85, pady=5)

## 13. Output folder
lblOutput = Label(window, text="Adversarial images output folder:")
lblOutput.grid(column=2, row=7, sticky="W", padx=10, pady=5)
txtOutput = Entry(window, width=20)
txtOutput.insert(tk.END, './logs')
txtOutput.grid(column=3, row=7, sticky="W", padx=10, pady=5)
btnOutput = Button(window, text="Browse...", command=get_directory)
btnOutput.grid(column=4, row=7, sticky="W", padx=10, pady=5)

## 14. URL string & Initial QR Code 
lblURL = Label(window, text="QR code URL string:")
lblURL.grid(column=2, row=8, sticky="W", padx=10, pady=5)
comboURL = Combobox(window, state="readonly", width=25)
comboURL['values'] = ("https://www.google.com", "https://www.facebook.com", "https://www.stackoverflow.com", "https://www.reddit.com", "https://www.twitter.com")
comboURL.current(0)  # set the selected item
comboURL.grid(column=3, row=8, sticky="W", padx=10, pady=5)
comboURL.bind("<<ComboboxSelected>>", comboURLCallback)
qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_H,
    box_size=10,
    border=4,
    )
qr = qrcode.make(comboURL.get())
qr = qr.resize((128, 128), Image.ANTIALIAS)
render = ImageTk.PhotoImage(qr)
lblImg = Label(window, image=render)
lblImg.image = render
lblImg.grid(column=3, row=9, sticky="W", padx=10)

## 15. Target class
lblImgClass = Label(window, text="Target image class:")
lblImgClass.grid(column=0, row=8, sticky="W", padx=10, pady=5)
test_list = [line.rstrip('\n') for line in open('./idx_labels.txt')]
frm = Frame(window)
frm.grid(column=0, row=9, columnspan=5, sticky="W", padx=10, pady=5)
txtImgClass = Entry(frm, width=80)
txtImgClass.insert(tk.END, test_list[859])
txtImgClass.bind('<KeyRelease>', on_keyrelease)
txtImgClass.pack(side=tk.TOP)
scrollbar = Scrollbar(frm, orient="vertical")
scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
listImgClass = tk.Listbox(frm, width=78, yscrollcommand=scrollbar.set, exportselection=False)
listImgClass.bind('<<ListboxSelect>>', on_select)
listImgClass.pack(side=tk.LEFT, fill=tk.Y)
scrollbar.config(command=listImgClass.yview)
listbox_update(test_list)

## 16. Console
lblConsole = Label(window, text="Console:")
lblConsole.grid(column=0, row=10, sticky="W", padx=10, pady=5)
console = scrolledtext.ScrolledText(window, width=120, height=11, undo=False, state=tk.DISABLED)
console.grid(column=0, row=11, columnspan=5, sticky="NW", padx=10, pady=5)

## 17. Button to execute or terminate
frm3 = Frame(window)
frm3.grid(column=4, row=9, columnspan=5, sticky="W", padx=10, pady=5)
btnGo = Button(frm3, text="Go", command=gocallback)
btnGo.grid(column=0, row=0)
btnTerminate = Button(frm3, text="Terminate", command=terminatecallback)
btnTerminate.grid(column=0, row=1)
window.bind("<Enter>", empty_check)

# Execute the window's main loop
window.mainloop()
