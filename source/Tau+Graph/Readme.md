(Examples of each variable, generated QR patches & adversarial examples, and graph included)
1. Put QR patch in the same directory as tau.py & change string of patchFileName in tau.py
2. Put testing images in /input
3. Run tau.py
4. Run graph.py