import pandas as pd
import glob
import matplotlib.pyplot as plt
import numpy as np

filesCir = glob.glob("csv/*_c.csv")
filesSqu = glob.glob("csv/*_s.csv")
len = float(len(filesCir))
#print(len)
li = []

for filename in filesCir:
    df = pd.read_csv(filename, index_col=None, header=0)
    li.append(df)

frame = pd.concat(li, axis=0, ignore_index=True)
#print(frame)
frameSum = frame.groupby(['tau'],as_index=False).sum()
sem = frame.groupby(['tau'],as_index=False).sem()
#print(sem)
#print(frameSum)
frameSum = frameSum.assign(prob_SD = sem["prob"])
frameSum = frameSum.assign(loss_SD = sem["loss"])
frameSum['loss'] = frameSum['loss'].div(len)#.round(2)
frameSum['prob'] = frameSum['prob'].div(len)

li2 = []
for filename in filesSqu:
    df = pd.read_csv(filename, index_col=None, header=0)
    li2.append(df)

frame2 = pd.concat(li2, axis=0, ignore_index=True)
#print(frame)
frameSum2 = frame2.groupby(['tau'],as_index=False).sum()
sem2 = frame2.groupby(['tau'],as_index=False).sem()
#print(sem)
#print(frameSum)
frameSum2 = frameSum2.assign(prob_SD = sem2["prob"])
frameSum2 = frameSum2.assign(loss_SD = sem2["loss"])
frameSum2['loss'] = frameSum2['loss'].div(len)#.round(2)
frameSum2['prob'] = frameSum2['prob'].div(len)

print(len)
ax = frameSum.plot(kind="line", x="tau", y="prob", color = "blue", label = "Circle QR") #y=["loss", "prob"])
ax.errorbar(frameSum["tau"], frameSum["prob"], yerr = frameSum["prob_SD"], 
             ecolor = "blue", capsize = 3, errorevery = 10, markevery = 10, fmt = "db", alpha = 0.6)

frameSum2.plot(kind="line", x="tau", y="prob", ax = ax, color = "green", label = "Square QR", linestyle='dashed') #y=["loss", "prob"])
ax.errorbar(frameSum2["tau"], frameSum2["prob"], yerr = frameSum2["prob_SD"], 
             ecolor = "green", capsize = 3, errorevery = 10, markevery = 10, fmt = "og", alpha = 0.6)

ax.set_xlabel("Tau Value")
ax.set_ylabel("Probability of \"Panpipe\" Class")

plt.savefig('graphprob.png')
plt.show()

'''
ax2 = frameSum.plot(kind="line", x="tau", y="loss") #y=["loss", "prob"])
ax2.errorbar(frameSum["tau"], frameSum["loss"], yerr = frameSum["loss_SD"], 
             ecolor = "black", capsize = 2, errorevery = 5, markevery = 5, fmt = "d", alpha = 0.6)
ax2.set_xlabel("Tau")
ax2.set_ylabel("Loss")
plt.savefig('graphloss.png')
plt.show()
'''
