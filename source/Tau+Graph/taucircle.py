from PIL import Image, ImageOps
import numpy as np
import qrcode
import torch
import pretrainedmodels
import pretrainedmodels.utils as utils
import torch.nn.functional as F
import sys
import glob
import os

def progressbar(it, prefix="", size=60, file=sys.stdout):
    count = len(it)
    def show(j):
        x = int(size*j/count)
        file.write("%s[%s%s] Tau: %i/%i\r" % (prefix, "#"*x, "."*(size-x), j, count))
        file.flush()        
    show(0)
    for i, item in enumerate(it):
        yield item
        show(i+1)
    file.write("\n")
    file.flush()

patchFileName = "patchcircle.png"
urlString = 'http://clockup.net'
target = 699 #959 = carbonara, 699 = panpipe
netName = 'inceptionv3'
radius = 50

print("Creating target model")
netClassifier = pretrainedmodels.__dict__[netName](num_classes=1000, pretrained='imagenet')
netClassifier.eval()
tf_img = utils.TransformImage(netClassifier)

print("Creating QR code & binary array")
qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_H,
    box_size=10,
    border=4,
    )
qr = qrcode.make(urlString) #URL string goes here, creates 1-bit (0,1) Image
qrSize = int(np.floor(radius*2*0.85))
if(qrSize % 2 == 1): qrSize -= 1
qr = qr.resize((qrSize,qrSize))
qr = qr.convert('RGB')
arrQR = np.array(qr).astype(bool)
padSize = int(((radius*2)-qrSize)/2)
arrQR = np.pad(arrQR, ((padSize,padSize),(padSize,padSize), (0, 0)), 'constant', constant_values=(1))
arrQR = arrQR.astype(np.uint8)
#print("\n-----arrQR------\n", arrQR)
Image.fromarray(arrQR.astype(np.uint8)*255).save('0_qr_c.png')

### Creating binary patch
print("Creating binary patch")
patch = Image.open(patchFileName).convert('RGB')
patchArr = np.array(patch)
#print("\n-----patchArr------\n", patchArr)
patchBin = np.ceil(patchArr.astype(np.float16) / 255.0)
#print("\n-----patchBin------\n", patchBin)
patchBin = patchBin.astype(np.bool)
patchBinInv = np.invert(patchBin)
patchBinInv = patchBinInv.astype(np.uint8)
#print("\n-----patchBinInv------\n", patchBinInv)
Image.fromarray(patchBin.astype(np.uint8)*255).save('1_patchBin_c.png')
Image.fromarray(patchBinInv*255).save('2_patchBinInv_c.png')

mask = Image.open('mask.png').convert('L')
for filename in glob.glob('input/*.png'):
    print(os.path.basename(filename))
    advEx = Image.open(filename).convert('RGB')
    arr = np.empty((0,3))
    for tau in progressbar(range(90), 'Progress: '):
        ### Adding tau & QR data
        patchTau = patchArr + (patchBinInv * tau)
        #print("\n-----patchTau------\n", patchTau)
        patchQR = patchTau * arrQR
        patchImg = Image.fromarray(patchQR)
        patchImg = ImageOps.fit(patchImg, mask.size, centering=(0.5, 0.5))
        patchImg.putalpha(mask)
        patchImg.save(('patch/'+patchFileName.split(".")[0]+'_tau_c_'+str(tau)+'.png'))

        ### Applying patch to input image
        #pos = (advEx.size[0] - patchImg.size[0], advEx.size[1] - patchImg.size[1])
        pos = (80, 160)
        advEx.paste(patchImg, pos, patchImg)
        advEx.save(('advEx/'+os.path.basename(filename).split(".")[0]+'_adv_c_'+str(tau)+'.png'))

        ### Evaluation
        input_tensor = tf_img(advEx)
        input_tensor = input_tensor.unsqueeze(0) # 3x299x299 -> 1x3x299x299
        input = torch.autograd.Variable(input_tensor, requires_grad=True)
        prediction = netClassifier(input) # 1x1000
        out = F.softmax(prediction)
        prob = out.data[0][target]
        #prob = 0
        #adv_out = F.log_softmax(prediction)
        #loss = -adv_out[0][target]
        loss = 0
        arr = np.append(arr, [[tau,loss,prob]], axis=0)

    ### Saving into csv file
    ###[Tau, Loss, Probability]
    #print(arr)
    temp = [["tau","loss","prob"]]
    np.savetxt(('csv/'+os.path.basename(filename).split(".")[0]+'_eval_c.csv'), temp, delimiter=",", fmt=['%s', '%s', '%s']) 
    with open(('csv/'+os.path.basename(filename).split(".")[0]+'_eval_c.csv'), "ab") as f:
        np.savetxt(f, arr, delimiter=",", fmt=['%d', '%f', '%f'])

"""
#print(prediction.data)
#print("Max class: ", prediction.data.max(1), "\n") #indices = class idx
print("\nCrossentropy loss of Specified class: ", loss)
#print("Probability of Max class: ", out.max(1), "\n")
print("\nProbability of Specified class: ", prob)
print(prediction.data[0][target].numpy())
print(out.data[0][target].numpy())
"""