from PIL import Image
import numpy as np
import qrcode
import torch
import pretrainedmodels
import pretrainedmodels.utils as utils
import torch.nn.functional as F
import sys
import glob
import os

def progressbar(it, prefix="", size=60, file=sys.stdout):
    count = len(it)
    def show(j):
        x = int(size*j/count)
        file.write("%s[%s%s] Tau: %i/%i\r" % (prefix, "#"*x, "."*(size-x), j, count))
        file.flush()        
    show(0)
    for i, item in enumerate(it):
        yield item
        show(i+1)
    file.write("\n")
    file.flush()

patchFileName = "patchsquare.png"
urlString = 'http://clockup.net'
target = 699 #959 = carbonara, 699 = panpipe
netName = 'inceptionv3'

print("Creating target model")
netClassifier = pretrainedmodels.__dict__[netName](num_classes=1000, pretrained='imagenet')
netClassifier.eval()
tf_img = utils.TransformImage(netClassifier)

print("Creating QR code & binary array")
qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_H,
    box_size=10,
    border=4,
    )
qr = qrcode.make(urlString) #URL string goes here, creates 1-bit (0,1) Image
qr = qr.resize((74,74))
qr = qr.convert('RGB')
arrQR = np.array(qr).astype(np.bool)
arrQR = arrQR.astype(np.uint8)
#print("\n-----arrQR------\n", arrQR)
qr.save("0_qr_s.png")

### Creating binary patch
print("Creating binary patch")
patch = Image.open(patchFileName).convert('RGB')
patchArr = np.array(patch)
#print("\n-----patchArr------\n", patchArr)
patchBin = np.ceil(patchArr.astype(np.float16) / 255.0)
#print("\n-----patchBin------\n", patchBin)
patchBin = patchBin.astype(np.bool)
patchBinInv = np.invert(patchBin)
patchBinInv = patchBinInv.astype(np.uint8)
#print("\n-----patchBinInv------\n", patchBinInv)
Image.fromarray(patchBin.astype(np.uint8)*255).save('1_patchBin_s.png')
Image.fromarray(patchBinInv*255).save('2_patchBinInv_s.png')

for filename in glob.glob('input/*.png'):
    print(os.path.basename(filename))
    advEx = Image.open(filename).convert('RGB')
    arr = np.empty((0,3))
    for tau in progressbar(range(90), 'Progress: '):
        ### Adding tau & QR data
        patchTau = patchArr + (patchBinInv * tau)
        #print("\n-----patchTau------\n", patchTau)
        patchQR = patchTau * arrQR
        #Image.fromarray(patchTau).save('3_patchTau.png')
        Image.fromarray(patchQR).save(('patch/'+patchFileName.split(".")[0]+'_tau_s'+str(tau)+'.png'))

        ### Applying patch to input image
        patchImg = Image.fromarray(patchQR)
        #pos = (advEx.size[0] - patchImg.size[0], advEx.size[1] - patchImg.size[1])
        pos = (80, 160)
        advEx.paste(patchImg, pos)
        advEx.save(('advEx/'+os.path.basename(filename).split(".")[0]+'_adv_s_'+str(tau)+'.png'))

        ### Evaluation
        input_tensor = tf_img(advEx)
        input_tensor = input_tensor.unsqueeze(0) # 3x299x299 -> 1x3x299x299
        input = torch.autograd.Variable(input_tensor, requires_grad=True)
        prediction = netClassifier(input) # 1x1000
        out = F.softmax(prediction)
        prob = out.data[0][target]
        #prob = 0
        #adv_out = F.log_softmax(prediction)
        #loss = -adv_out[0][target]
        loss = 0
        arr = np.append(arr, [[tau,loss,prob]], axis=0)

    ### Saving into csv file
    ###[Tau, Loss, Probability]
    #print(arr)
    temp = [["tau","loss","prob"]]
    np.savetxt(('csv/'+os.path.basename(filename).split(".")[0]+'_eval_s.csv'), temp, delimiter=",", fmt=['%s', '%s', '%s']) 
    with open(('csv/'+os.path.basename(filename).split(".")[0]+'_eval_s.csv'), "ab") as f:
        np.savetxt(f, arr, delimiter=",", fmt=['%d', '%f', '%f'])

"""
#print(prediction.data)
#print("Max class: ", prediction.data.max(1), "\n") #indices = class idx
print("\nCrossentropy loss of Specified class: ", loss)
#print("Probability of Max class: ", out.max(1), "\n")
print("\nProbability of Specified class: ", prob)
print(prediction.data[0][target].numpy())
print(out.data[0][target].numpy())
"""